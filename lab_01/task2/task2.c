﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <windows.h>
#include <math.h>
union number {
	signed short value;
	unsigned char bytes[sizeof(short)];
};
void printNumberUseUnion(signed short number) {
	union number myNumber;
	myNumber.value = number;
	printf("Sign: %d\n", (myNumber.value > 0) ? 1 : -1);
	printf("Value: %d", abs(myNumber.value));
}
int getSign(signed short number) {
	const int BIT_NUM = 8;

	return (number != 0) | number >> (sizeof(short) * BIT_NUM - 1);
}
void printLogicOperator(signed short number) {
	printf("\n\nSign: %d\n", getSign(number));
	printf("Value: %d", abs(number));
}
int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	printf("Введіть число: ");
	int number;
	scanf("%d", &number);
	printNumberUseUnion(number);
	printLogicOperator(number);
	return 0;
}
