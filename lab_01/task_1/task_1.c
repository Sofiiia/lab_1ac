﻿
#include <stdio.h>
#include <time.h>
typedef struct {
    int year:12;   
    int month:12;  
    int day:6;     
    int hour:5;    
    int minute:6;  
    int second:13;  
} Date;
typedef struct {
    signed short value;
    unsigned short sign;
}Signnum;



void printDate(Date str) {
    printf("%04d-%02d-%02d %02d:%02d:%02d\n", str.year, str.month, str.day, str.hour, str.minute, str.second);
}
union CompactFloat {
    float f;            
    unsigned char bytes[sizeof(float)]; 
};
int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    Date str = { 2024, 2, 13, 15, 30, 45 };

    printf("Розмір структури tm: %zu байт(и)\n", sizeof(struct tm));
    printf("Розмір структури Date: %zu байт(и)\n", sizeof(str));
    printDate(str);

}
