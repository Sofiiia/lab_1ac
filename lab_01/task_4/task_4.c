﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <windows.h>
union CompactFloat {
	float value;
	unsigned char bytes[sizeof(float)];
};
void printFloat(float value) {
	union CompactFloat number;
	number.value = value;
	int bits[32];
	int* p;
	p = &number.value;
	for (int i = sizeof(int) * 8 - 1, j = 0; i >= 0; i--, j++)
	bits[j] = (*p) >> i & 1;
	printf("1.Побітово: ");
	for (int i = 0; i < 32; i++)
		printf("%d", bits[i]);
	printf("\n");
	printf("2.Побайтово: ");
	for (int i = 8; i <= 32; i += 8) {
		int j = 1, k = 0;
		for (int q = i - 1; i - 8 <= q; q--, j *= 2)
			if (bits[q])
				k+= j;
		printf("%02x ", k);
	}
	printf("\n");
	printf("3.Знак: %d\n", bits[0]);
	printf("4.Мантиса: ");
	for (int k = 9; k < 32; k++)
		printf("%d", bits[k]);
	printf("\n");
	printf("5.Cтупінь: ");
	for (int i = 1; i < 9; i++)
		printf("%d", bits[i]);
	printf("\n");
	printf("6.Обсяг пам'яті: %lu байт\n", sizeof(number));
}
int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	printf("Введіть число: ");
	float num;
	scanf("%f", &num);
	printFloat(num);
	return 0;
}