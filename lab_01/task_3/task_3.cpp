﻿#include <stdio.h>
#include <windows.h>
int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	signed char a, b, c, d, e, f;
	a = 5 + 127;
	b = 2 - 3;
	c = -120 - 34;
	d = (unsigned char)-5;
	e = 56 & 38;
	f = 56 | 38;
	printf("a) %x\nб) %x\nв) %x\nг) %x\nд) %x\nе) %x", a, b, c, d, e, f);
	return 0;
}